import { Component } from '@angular/core';
import { NavController, NavParams, AlertController} from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import {Answer} from "../../components/Answer";
import {maindb} from '../../providers/maindb';
import { BrainfightCreate } from '../brainfight-create/brainfight-create'
import { BrainfightListPage } from '../brainfight-list/brainfight-list'
/**
 * Generated class for the Create page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

const URL = "http://localhost:8080/profile";

@Component({
  selector: 'page-create',
  templateUrl: 'create.html',
})


export class CreatePage {
  public myGroup: FormGroup;
  public uploader:FileUploader = new FileUploader({url: URL});
  dataState = {};
  questionTitle: any;
  showIcon = true;
  myImage = "testas";
  hideQuestions = false;
  b = "data";
  question: string;
  model = "Pridėti klausimą";
  answers: Answer[] = [
    {
      text: "Nothing",
      selected: false
    },
    {
      text: "Nothing2",
      selected: false
    },
    {
      text: "Nothing3",
      selected: false
    },
    {
      text: "Nothing4",
      selected: false
    },
  ];
  constructor(private _fb: FormBuilder, public db: maindb, public navCtrl: NavController, public navParams: NavParams, public reviewService: maindb, private alertCtrl: AlertController) {
    this.myGroup = new FormGroup({
      firstName: new FormControl(),
      filas: new FormControl()
    });
  }
  hidePlaceholder(){
    this.model = "";
    this.showIcon = false;
  }
  submitQuestion(){

  }
  onChange($event){
    this.readThis($event);
  }
    // console.log('bandymas' + $event.srcElement.files[0]);
    // var reader = new FileReader();
    // console.log("WOT");
    // reader.setImage = function (text) {
    //     console.log(this.myImage);
    //     this.myImage = text;
    //     return 'yay';
    // };
    // reader.onload = function (this,e) {
    //     debugger;
    //     // that.myImage = e.target.result;
    // }
    // reader.readAsDataURL($event.srcElement.files[0]);
  
  readThis(inputValue: any) : void {
    var file:File = inputValue.target.files[0];
    var myReader:FileReader = new FileReader();

    myReader.onload = function(e){
      let el = <HTMLImageElement>document.querySelector(".file-image");
      el.src = this.result;
      let plus = <HTMLImageElement>document.querySelector(".file-plus");
      plus.style.display = "none";
    };

    myReader.readAsDataURL(file);
  }
  home(){
    this.navCtrl.pop();
  }
  info(){
    let alert = this.alertCtrl.create({
      title: 'Pateikti klausimą',
      subTitle: 'Čia galite pateikti klausimą ir taip prisidėti prie testų gerinimo!',
      buttons: ['Gerai']
    });
      alert.present();
  }
  submit(){
    //checks if has info from brainfight, adds question to datastate,
    //sets state to another question and goes back to brainfight make
    if(this.navParams.get("dataState")){
      var dataState = this.navParams.data.dataState;
      var stepState = this.navParams.data.stepState;
      var maxQuestion = dataState.etapai[stepState.etapas].klausimaiSk;
      var maxEtapai = dataState.etapuSk;
      var currentQuestion = (stepState.klausimas + 1);
      var currentEtapas = (stepState.etapas + 1);
      dataState.etapai[stepState.etapas].klausimai[stepState.klausimas] = {question: this.question, answers: this.answers, tipas: this.hideQuestions};
      
      //if it is final question and etapas
      if(currentQuestion == maxQuestion && currentEtapas == maxEtapai){
        this.db.createQuiz(dataState).subscribe((res) => {
            this.navCtrl.push(BrainfightListPage,{data: dataState})
        })
        return
      }


      //if the question isnt last in etapai
      if(currentQuestion < maxQuestion){
        stepState.klausimas += 1;
      }else{
        //if last question in etapas
        stepState.current = 5;
        stepState.klausimas = 0;
      }

  
      this.navCtrl.push(BrainfightCreate,{dataState: dataState, stepState: stepState});
      
      
      // if(stepState.klausimas == stepState.k)
      
      // sends data back
     
    }
    console.log("submitclick");
    //collect answers to id
    let solutions = [];
    let solutionsStr = "";
    this.answers.forEach(function(el,i){
      if(el.selected){
        solutions.push(i+1);
      }
    });

    solutionsStr = solutions.toString();
    let form = {answers: this.answers, question: this.question, solution: solutionsStr};
    if(!this.navParams.get("dataState")){
      this.reviewService.createReview(form).subscribe((res) => {
        let alert = this.alertCtrl.create({
          title: 'Tavo klausimas sėkmingai išsiųstas!',
          subTitle: 'Aplikacija tau praneš kada tavo klausimą patvirtins moderatorius!',
          buttons: ['Gerai']
        });
        alert.present();
      })
    }
  }
  ionViewDidLoad(navParams: NavParams) {
    if(this.navParams.get("dataState")){
      //hide answers if question is open
      this.hideQuestions = this.navParams.data.hideQuestions;
      
      
    }
  

  }

}
