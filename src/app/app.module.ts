import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { KurtiPage } from '../pages/kurti/kurti'
import { HighscorePage } from '../pages/highscore/highscore'
import { ZaistiPage } from '../pages/zaisti/zaisti'
import { Brainfight } from '../pages/brainfight/brainfight'
import { BrainfightCreate } from '../pages/brainfight-create/brainfight-create'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TestDonePage } from '../pages/test-done/test-done';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import {BrainfightListPage} from '../pages/brainfight-list/brainfight-list' 
import { QuestionListPage } from '../pages/question-list/question-list'
import { FileSelectDirective } from 'ng2-file-upload';
import {CreatePage} from "../pages/create/create";

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    KurtiPage,
    Brainfight,
    QuestionListPage,
    BrainfightCreate,
    HomePage,
    CreatePage,
    ZaistiPage,
    HighscorePage,
    BrainfightListPage, 
    FileSelectDirective,
    TabsPage,
    TestDonePage
  ],
  imports: [
    BrowserModule, 
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HighscorePage,
    KurtiPage,
    CreatePage,
    Brainfight,
    BrainfightCreate,
    ContactPage,
    HomePage,
    QuestionListPage,
    BrainfightListPage,
    ZaistiPage,
    TabsPage,
    TestDonePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
