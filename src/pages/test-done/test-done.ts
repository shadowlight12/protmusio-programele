import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ZaistiPage } from '../zaisti/zaisti';
/*
  Generated class for the TestDone page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-test-done',
  templateUrl: 'test-done.html'
})
export class TestDonePage {
  testData: any;
  test: any;
  finalData: any;
  correct: number;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.correct = 0;
    this.testData = navParams.get("testData");
    this.test = navParams.get("test");
    this.finalData = navParams.get("finalData");
    // var solutions = [];
    for(let i  = 0; i < this.testData.length; i++){
      if(this.testData[i][4] == true){
        this.correct++;
      }
    }
    for(let i = 0; i < this.testData.length; i++){
      this.items.push({number: i, correct: this.testData[i][4]})
    } 
  }
  items = [];

  jumpToQuestion(question){
    this.navCtrl.push(ZaistiPage,{state: "check",question: question, data: this.test, testData: this.testData, finalData: this.finalData} );
  }
  newTest(){
    this.navCtrl.push(ZaistiPage, {state: "test"});
  }
  sameTest(){
    this.navCtrl.push(ZaistiPage,{state: "test", data: this.test});
  }
  closeTest(){
    this.navCtrl.push(HomePage);
  }
  ionViewDidLoad(navParams: NavParams) {
    console.log('ionViewDidLoad TestDonePage test');
    let testData = navParams.get("testData");
  
  }

}
