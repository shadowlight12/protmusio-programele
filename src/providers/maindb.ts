import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

/*
 Generated class for the maindb provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class maindb {

  data:any;
  constructor(public http: Http) {
    this.data = null;
  }

  getReviews(){

    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {

      this.http.get('http://192.168.0.101:8080/api/questions')
          .map(res => res.json())
          .subscribe(data => {
            this.data = data;
            resolve(this.data);
          });
    });

  }
  uploadFile(data){
    console.log("upload start");
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://192.168.0.101:8080/profile', JSON.stringify({avatar: data}), {headers: headers});
  }
  createQuiz(quiz){
    let headers = new Headers()
    headers.append('Content-Type', 'application/json')
    return this.http.post('http://192.168.0.101:8080/postQuiz',quiz)
  }
  createReview(review){

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

   return this.http.post('http://192.168.0.101:8080/api/questions', JSON.stringify(review), {headers: headers});
        
  }

  deleteReview(id){
    this.http.delete('http://192.168.0.101:8080/api/reviews/' + id).subscribe((res) => {
      console.log(res.json());
    });

  }

}
