import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the Highscore page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-highscore',
  templateUrl: 'highscore.html',
})
export class HighscorePage {
  items = ["user1 700", "user1 500", "user1 500", "user1 500", "user1 500", "user1 500", "user1 200", "user1 200", "user1 200", "user1 200", "user1 100"  ];
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
    
  }
  home(){
    this.navCtrl.pop();
  }
  info(){
    let alert = this.alertCtrl.create({
        title: 'Rekordai',
        subTitle: 'Čia galite peržiūrėti savo geriausius rezultatus',
        buttons: ['Gerai']
      });
      alert.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Highscore');
  }

}
