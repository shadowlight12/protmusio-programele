import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TestDonePage} from '../test-done/test-done';
import {maindb} from '../../providers/maindb';
/*
 Generated class for the Zaisti page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-zaisti',
    templateUrl: 'zaisti.html'
})
export class ZaistiPage {
    item1: any;
    item2: any;
    item3: any;
    item4: any;
    _this: any;
    currentQuestion = 0;
    solutionID: number;
    answers = [];
    data: any;
    finalData = [];
    condition: boolean;
    question: number;
    state: string;
    testas = 4;
    questions = [
        {id: 1, question: "klausimas 1", Answ1: "a", Answ2: "b", Answ3: "c", Answ4: "d", solutionID: 0},
        {id: 2, question: "klausimas 2", Answ1: "a", Answ2: "b", Answ3: "c", Answ4: "d", solutionID: 0},
    ];
    reviews: any;

    constructor(public navCtrl: NavController, public reviewService: maindb, public navParams: NavParams) {
        console.log('ionViewDidLoad ZaistiPage');

        this.question = navParams.get("question");
        this.state = navParams.get("state");
        if (navParams.get("testData")) {
            this.answers = navParams.get("testData");
            console.log(this.answers);
        }
        
        try {
            this.data = navParams.get("data");

        }
        catch (e) {
            console.log("klaida", e);
        }
        if(this.state == "check"){
            this.finalData = navParams.get("finalData");
        }
    }

    setAnswers() {
        this.item1 = this.answers[this.currentQuestion][0];
        this.item2 = this.answers[this.currentQuestion][1];
        this.item3 = this.answers[this.currentQuestion][2];
        this.item4 = this.answers[this.currentQuestion][3];
    }

    nextQuestion() {
        console.log(this.currentQuestion);
        //logic for checking
        //checks if questions were already test
        if (this.state == "test") {
            this.answers.push([this.item1 || false, this.item2 || false, this.item3 || false, this.item4 || false]);
            var condition = true;
            var fData = [];
            for (let i = 0; i < this.answers[this.answers.length - 1].length; i++) {
                let element = this.answers[this.answers.length - 1][i];
                if (element == true) {
                    if (i + 1 != this.questions[this.currentQuestion].solutionID) {
                        fData.push(false);
                        condition = false;
                    } else {
                        fData.push(true);
                    }
                } else {
                    if (i + 1 == this.questions[this.currentQuestion].solutionID) {
                        fData.push(false);
                        condition = false;
                    } else {
                        fData.push(true);
                    }
                }
            }
            this.finalData.push(fData);

            this.answers[this.answers.length - 1][4] = condition;
            this.item1 = false;
            this.item2 = false;
            this.item3 = false;
            this.item4 = false;
        }

        if (this.currentQuestion < (this.questions.length - 1)) {
            this.currentQuestion++;
            if (this.state == "check") {
                this.setAnswers();

            }
        } else {
            console.log("data: ", this.finalData);
            this.navCtrl.push(TestDonePage, {testData: this.answers, test: this.questions, finalData: this.finalData});
        }
    }

    test() {
        console.log("failed");
    }

    closeQuestions() {
        this.navCtrl.pop();
    }

    ionViewDidLoad(questions, navParams: NavParams) {
        this.reviewService.getReviews().then((dataB) => {
            let a = dataB;
            for (let i = a.length; i; i--) {
                let j = Math.floor(Math.random() * i);
                [a[i - 1], a[j]] = [a[j], a[i - 1]];
            }

            if (this.data && this.data.length > 0) {
                a = this.data;
            } else {
                a = a.splice(0, 30);
            }
            this.questions = a;
            if (this.question) {
                this.currentQuestion = this.question;
                console.log(this.currentQuestion);
            }
            if (this.state == 'check') {
                this.setAnswers();
            }
        });
    }

}
