import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { CreatePage } from '../create/create';
import { HomePage } from  '../../pages/home/home'
import {maindb} from '../../providers/maindb';
/**
 * Generated class for the BrainfightCreate page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-brainfight-create',
  templateUrl: 'brainfight-create.html',
})
export class BrainfightCreate {
  stepState = {current: 1, max: 5, min: 1, etapas: 0, klausimas: 0};
  dataState = {etapuSk: null, etapai: []};
  constructor(public navCtrl: NavController, public navParams: NavParams, public db: maindb, private alertCtrl: AlertController) {
  }
  home(){
    this.navCtrl.push(HomePage);
  }
  info(){
    let alert = this.alertCtrl.create({
        title: 'Kurti protmūšį',
        subTitle: 'Čia galite sukurti protmūšį',
        buttons: ['Gerai']
      });
      alert.present();
  }
  fillQuestion(questionStatus){
    //sets question status (open or closed) to dataState and then sends to createpage
    this.dataState.etapai[this.stepState.etapas].klausimai[this.stepState.klausimas].open = questionStatus;
    this.navCtrl.push(CreatePage,{dataState: this.dataState, stepState: this.stepState, hideQuestions: questionStatus});
  }
  ionViewDidLoad() {
    //load state if it comes from other page
    if(this.navParams.get("dataState")) {
      this.dataState = this.navParams.data.dataState;
      this.stepState = this.navParams.data.stepState;
    }
    var maxEtapai = this.dataState.etapuSk;
    var currentEtapas = (this.stepState.etapas + 1);
    if(currentEtapas == maxEtapai && this.stepState.current == this.stepState.max){
      console.log(this.dataState)
        
    }
  }
  prevState(){
    if(this.stepState.current > this.stepState.min){
      this.stepState.current -= 1;
    }
  }
  nextState(){
    if(this.stepState.current == 1){
      for(var i = 0; i < this.dataState.etapuSk; i++){
        this.dataState.etapai.push({laikas: {min: null, sec: null}, klausimaiSk: null, klausimai: []});
      }
      this.stepState.current += 1; 
      return;
    }
    if(this.stepState.current == 3){
      for(var i = 0; i < this.dataState.etapai[this.stepState.etapas].klausimaiSk; i++){
        this.dataState.etapai[this.stepState.etapas].klausimai.push({tipas: null});
      }
    }
    if(this.stepState.current < this.stepState.max){
      this.stepState.current += 1;
    }
  }
  nextEtapas(){
    var maxEtapai = this.dataState.etapuSk;
    var currentEtapas = (this.stepState.etapas + 1);
    if(currentEtapas == maxEtapai){
      console.log(this.dataState)
      this.db.createQuiz(this.dataState).subscribe((res) => {
        alert("Protmūšis įdėtas sėkmingai")
      this.navCtrl.pop();
    })
    }else{
      this.stepState.etapas++;
      this.stepState.current = 2;
    }
    
  }

}
