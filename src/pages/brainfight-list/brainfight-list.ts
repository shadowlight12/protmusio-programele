import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import { HomePage } from  '../../pages/home/home'
/**
 * Generated class for the BrainfightListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-brainfight-list',
  templateUrl: 'brainfight-list.html',
})
export class BrainfightListPage {
  link: any;
  a = [1, 2, 3, 4];
  state: any;
  dataState: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
    this.dataState = this.navParams.data;
  }
  home() {
    this.navCtrl.push(HomePage);
  }
  copy() {
    this.link = 'localhost:3000/quiz/12'
    let alert = this.alertCtrl.create({
      title: 'Nuoroda nukopijuota',
      subTitle: 'Protmūšį taip pat galite pasiekti nuoroda \n' + this.link,
      buttons: ['Gerai']
    });
    alert.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad BrainfightListPage');
  }

}
