import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { BrainfightCreate } from '../brainfight-create/brainfight-create'
/**
 * Generated class for the Brainfight page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-brainfight',
  templateUrl: 'brainfight.html',
})
export class Brainfight {
  a = [1,2,3,4]; 
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
    
  }
  home(){
    this.navCtrl.pop();
  }
  info(){
    let alert = this.alertCtrl.create({
        title: 'Kurti protmūšį',
        subTitle: 'Čia galite sukurti protmūšį',
        buttons: ['Gerai']
      });
      alert.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Brainfight');
  }
  createTest(){
    console.log("test");
    this.navCtrl.push(BrainfightCreate);
  }
}
