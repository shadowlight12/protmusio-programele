import { Component } from '@angular/core';
import { CreatePage } from '../create/create';
import { ZaistiPage } from '../zaisti/zaisti';
import { Brainfight } from '../brainfight/brainfight';
import { AboutPage } from '../about/about';
import { NavController } from 'ionic-angular';
import {HighscorePage} from "../highscore/highscore";
import { QuestionListPage } from '../question-list/question-list'
import {maindb} from '../../providers/maindb';
import {BrainfightListPage} from '../brainfight-list/brainfight-list'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public reviewService: maindb) {

  }
  apie(){
    // this.navCtrl.push(BrainfightListPage);
    this.navCtrl.push(AboutPage);
  }
  scores(){
    this.navCtrl.push(HighscorePage);
  }
  kurtiKlausima(){
    this.navCtrl.push(CreatePage); 
  }
  sprestiTesta(){
    this.navCtrl.push(ZaistiPage,{state: "test"})
  }
  brainfight(){
    this.navCtrl.push(Brainfight);
  }
  questionCheck(){
    this.navCtrl.push(QuestionListPage);
  }
}
